/*
This file is part of SystemTestPortal.
Copyright (C) 2017  Institute of Software Technology, University of Stuttgart

SystemTestPortal is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SystemTestPortal is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SystemTestPortal.  If not, see <http://www.gnu.org/licenses/>.
*/

package project

import (
	"gitlab.com/stp-team/systemtestportal-webapp/domain/user"
	"gitlab.com/stp-team/systemtestportal-webapp/domain/visibility"
)

// Permissions are all permissions that
// a user can have in a project
type Permissions struct {
	DisplayPermissions
	ExecutionPermissions
	CasePermissions
	SequencePermissions
	MemberPermissions
	SettingsPermissions
}

// DisplayPermissions are the permissions
// to display elements in the project
type DisplayPermissions struct {
	DisplayProject bool
}

// ExecutionPermissions are the permissions
// affecting the execution
type ExecutionPermissions struct {
	Execute bool
}

// CasePermissions are the permissions
// affecting the cases in a project
type CasePermissions struct {
	CreateCase    bool
	EditCase      bool
	DeleteCase    bool
	DuplicateCase bool
	AssignCase    bool
}

// SequencePermissions are the permissions
// affecting the sequences in a project
type SequencePermissions struct {
	CreateSequence    bool
	EditSequence      bool
	DeleteSequence    bool
	DuplicateSequence bool
	AssignSequence    bool
}

// MemberPermissions are the permissions
// affecting the members in a project
type MemberPermissions struct {
	EditMembers bool
}

// SettingsPermissions are the permissions
// affecting the settings of a project
type SettingsPermissions struct {
	EditProject     bool // Change name, description, image, visibility
	DeleteProject   bool
	EditPermissions bool
}

// GetPermissions returns the permissions of a user in a project.
//
// Every user can view public projects. Users that are a member of a public
// project have the permissions that are defined for his role.
//
// Signed-In users can view internal projects. Users that are a member
// of an internal projects have the permissions that are defined for his role.
//
// Users that are owner or members of a project can view a private project.
// Nobody else has access to these projects.
func (p *Project) GetPermissions(u *user.User) Permissions {
	if p == nil {
		return Permissions{}
	}

	switch p.Visibility {
	case visibility.Public: // Everyone can view
		r := p.getRole(u)
		if r == nil {
			return displayPermissions
		}
		return r.Permissions
	case visibility.Internal: // User has to be logged in
		if u != nil {
			r := p.getRole(u)
			if r == nil {
				return displayPermissions
			}
			return r.Permissions
		}
	case visibility.Private: // User has to be member
		r := p.getRole(u)
		if r == nil {
			return Permissions{}
		}
		return r.Permissions
	}

	return Permissions{}
}

// getRole returns the role of a user in a project.
// Returns nil if the user is nil or if
// the user does not exist in the project.
func (p *Project) getRole(u *user.User) *Role {
	if p == nil || u == nil {
		return nil
	}
	if ms, ok := p.UserMembers[u.ID()]; ok {
		if r, ok := p.Roles[ms.Role]; ok {
			return r
		}
	}
	return nil
}

// displayPermissions are the permissions for users
// that are only allowed to display the project
var displayPermissions = Permissions{
	DisplayPermissions: DisplayPermissions{
		DisplayProject: true,
	},
	ExecutionPermissions: ExecutionPermissions{
		Execute: false,
	},
	CasePermissions: CasePermissions{
		CreateCase:    false,
		EditCase:      false,
		DeleteCase:    false,
		AssignCase:    false,
		DuplicateCase: false,
	},
	SequencePermissions: SequencePermissions{
		CreateSequence:    false,
		EditSequence:      false,
		DuplicateSequence: false,
		DeleteSequence:    false,
		AssignSequence:    false,
	},
	MemberPermissions: MemberPermissions{
		EditMembers: false,
	},
	SettingsPermissions: SettingsPermissions{
		EditProject:     false,
		DeleteProject:   false,
		EditPermissions: false,
	},
}
