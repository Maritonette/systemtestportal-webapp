FROM frolvlad/alpine-glibc

# libc seems to be required..
# https://serverfault.com/questions/883625/alpine-shell-cant-find-file-in-docker#comment1139245_883625

ADD /stp.tar.gz /tmp/

RUN cp /tmp/stp /usr/sbin/ \
    && mkdir /usr/share/stp/ \
    && mkdir /var/stp/ \
    && if [[  -f "./config.ini" ]] ; then mkdir /etc/stp/ && cp "./config.ini" /etc/stp/ ; fi \
    && cp -r /tmp/templates/ /usr/share/stp/templates \
    && cp -r /tmp/static/ /usr/share/stp/static \
    && cp -r /tmp/migrations/ /usr/share/stp/migrations \
    && cp -r /tmp/data/ /usr/share/stp/data \
    && rm -r /tmp/*

EXPOSE 8080

CMD ["stp", "--basepath=/usr/share/stp/", "--data-dir=/usr/share/stp/data"]
