/*
This file is part of SystemTestPortal.
Copyright (C) 2017  Institute of Software Technology, University of Stuttgart

SystemTestPortal is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SystemTestPortal is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SystemTestPortal.  If not, see <http://www.gnu.org/licenses/>.
*/

package middleware

import (
	"net/http"

	"sort"

	"github.com/urfave/negroni"
	"gitlab.com/stp-team/systemtestportal-webapp/domain/comment"
	"gitlab.com/stp-team/systemtestportal-webapp/domain/id"
	"gitlab.com/stp-team/systemtestportal-webapp/web/errors"
)

// CommentsKey is used to retrieve comments from a request context.
const CommentsKey = "Comments"

// CommentStore provides an interface for retrieving comments
type CommentStore interface {

	// Get returns the comments for the test case or test sequence
	// with the given ID for the given project under the given container
	Get(id id.TestID) ([]*comment.Comment, bool)
}

// Comments is a middleware that can retrieve the comments from a request
// it requires the project as well as the container middleware to work.
func Comments(store CommentStore, isCase bool) negroni.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request, next http.HandlerFunc) {
		var tName string
		var err error
		if isCase {
			tName, err = getParam(r, TestCaseKey)
		} else {
			tName, err = getParam(r, testSequenceParamKey)
		}
		if err != nil {
			errors.Handle(err, w, r)
			return
		}
		pID, err := getProjectID(r.Context().Value(ProjectKey), r)
		if err != nil {
			errors.Handle(err, w, r)
			return
		}
		c, err := getComments(store, id.NewTestID(pID, tName, isCase))
		if err != nil {
			errors.Handle(err, w, r)
			return
		}
		AddToContext(r, CommentsKey, c)
		next(w, r)
	}
}

func getComments(store CommentStore, tID id.TestID) ([]*comment.Comment, error) {
	c, ok := store.Get(tID)
	if !ok {
		return nil, nil
	}
	sort.Sort(sortComments(c))

	return c, nil
}

type sortComments []*comment.Comment

func (s sortComments) Len() int { return len(s) }

func (s sortComments) Swap(i, j int) { s[i], s[j] = s[j], s[i] }

func (s sortComments) Less(i, j int) bool {
	return s[i].CreationDate.After(s[j].CreationDate)
}
