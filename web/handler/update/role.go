/*
This file is part of SystemTestPortal.
Copyright (C) 2017  Institute of Software Technology, University of Stuttgart

SystemTestPortal is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SystemTestPortal is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SystemTestPortal.  If not, see <http://www.gnu.org/licenses/>.
*/

package update

import (
	"encoding/json"
	"net/http"

	"gitlab.com/stp-team/systemtestportal-webapp/web/errors"
	"gitlab.com/stp-team/systemtestportal-webapp/web/handler"
)

const (
	errCouldNotDecodeRolesTitle = "Couldn't save roles."
	errCouldNotDecodeRoles      = "We were unable to decode the change to roles " +
		"send in your request. This ist most likely a bug. If you want please " +
		"contact us via our " + handler.IssueTracker + "."
)

// ProjectRolesPut handles the request to update the
// roles of a project
func ProjectRolesPut(projectAdder handler.ProjectAdder) func(w http.ResponseWriter, r *http.Request) {
	return func(w http.ResponseWriter, r *http.Request) {
		c := handler.GetContextEntities(r)
		if c.Project == nil {
			errors.Handle(c.Err, w, r)
			return
		}

		if err := json.NewDecoder(r.Body).Decode(&c.Project.Roles); err != nil {
			errors.ConstructStd(http.StatusBadRequest,
				errCouldNotDecodeRolesTitle, errCouldNotDecodeRoles, r).
				WithLog("Couldn't read labels from request.").
				WithStackTrace(1).
				WithCause(err).
				WithRequestDump(r).
				Respond(w)
			return
		}

		if err := projectAdder.Add(c.Project); err != nil {
			errors.Handle(err, w, r)
			return
		}

		w.WriteHeader(http.StatusOK)
	}
}
